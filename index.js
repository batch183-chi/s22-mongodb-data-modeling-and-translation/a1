{
	"_id" : "user001",
	"firstName" : "John",
	"lastName" : "Smith",
	"email" : "johnsmith@mail.com",
	"password" : "johnsmith123",
	"isAdmin" : true,
	"mobileNumber" : "09123456789",

}

{

	"_id" : "user002",
	"firstName" : "Jane",
	"lastName" : "Doe",
	"email" : "janedoe@mail.com",
	"password" : "janedoe123",
	"isAdmin" : false,
	"mobileNumber" : "09123123123",

}

{
	"_id" : "order001",
	"userId" : [user002],
	"transactionDate" : "2022-06-10",
	"status" : "paid",
	"total" : 2000,
}

{
	"_id" : "orderproducts001",
	"orderId" : [order001],
	'productId' : [product001],
	"quantity" : 2,
	"price" : 500,
	"subTotal" : 2000,
}

{
	"_id" : "product001",
	"name" : "T-shirt",
	'description' : "White Round Neck",
	"price" : 500,
	"stocks" : 1000,
	"isActive" : true,
	"SKU" : "052321-SMHMD"
}





